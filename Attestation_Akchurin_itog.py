import os,re,csv
import json,datetime,time
import asyncio
import requests
import aiohttp


start_time = datetime.datetime.now()

file = open(os.path.join('cities.txt'))
cities = []
for line in file:
    result = re.findall(r'[A-Z]+', line, flags=re.I)
    if len(result) > 0:
        cities.append(' '.join(result))

class SaveCityWeather:
    """Класс сохранения данных города

        Attributes
        ----------
        info_city : json()
            информация полученная с сайта openweathermap.org

        Methods
        -------
        save_in_file(path : str)
            path : str
            Путь, куда нужно сохранить
        """
    def __init__(self, info_city):
        self.name = info_city['name']
        self.temperature = info_city['main']['temp']
        self.humidity = info_city['main']['humidity']
        self.pressure = info_city['main']['pressure']
        self.wind = info_city['wind']["speed"]

    def save_in_file(self, path : str):
        direct_list = os.listdir(path)
        path += '/weather.txt'
        flag = 'w'
        if 'weather.txt' in direct_list:
            flag = 'a'
        with open(path, flag) as f:
            f.write(f"Город: {self.name}\tТемпература воздуха: {self.temperature} C°\tВлажность воздуха:\
            {self.humidity} %\tДавление: {self.pressure} мм.р.т.\tВетер: {self.wind} м/с\n")

async def find_weather(city):
    openweatherAPI = '0fb610dab7456bc44dbdde2ddba9be71'
    city_ = re.sub(" ", "%20", city)
    url = f'https://api.openweathermap.org/data/2.5/weather?q={city_}&appid={openweatherAPI}&units=metric'
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as request:
            return await request.json()


# async def main():
#
#     for city in cities:
#         dict_city = await find_weather(city)
#         save_city = SaveCityWeather(dict_city)
#         save_city.save_in_file('folder')



async def main(city):
    dict_city = await find_weather(city)
    save_city = SaveCityWeather(dict_city)
    save_city.save_in_file('folder')

async def main2():
    tasks = []
    for city in cities:
        task = asyncio.create_task(main(city))
        tasks.append(task)
    await asyncio.gather(*tasks)

if __name__ == "__main__":
    asyncio.run(main2())

finish_time = datetime.datetime.now() - start_time
print(f"Затраченное время на работу скрипта: {finish_time}")  # время 00.513780